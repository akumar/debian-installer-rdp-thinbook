#!/bin/bash

PACKAGES=$(dpkg-scanpackages extra-packages/|awk 'BEGIN { RS = "" } { print $2 }')
echo $PACKAGES
# Check if the package is in the overrides already
for i in $PACKAGES; do
    if grep -q "^$i" indices/override; then
	continue
    fi
    echo -e "$i\textra\tscience" >> indices/override
done
